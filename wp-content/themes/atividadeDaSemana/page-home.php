<?php
/*
Template Name: Homepage
*/
?>

<?php get_header() ?>

    <main>
        <?php $image = (get_field("imagem"));?>
        <img src="<?php echo $image["url"] ?>" alt="<?php echo $image["alt"] ?>" ?>>
        <h3><?php the_field("texto"); ?></h3>
        <p><?php the_field("area_de_texto"); ?></p>
        <?php get_template_part( "inc", "section"); ?>
    </main>

<?php get_footer() ?>