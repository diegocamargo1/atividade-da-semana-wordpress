<?php

    function wordpressAula2_scripts(){
        //wp_enqueue_style( "AtividadeDaSemana-reset-style", get_template_directory_uri() . "/CSS/reset.css" );
        wp_enqueue_style( "AtividadeDaSemana-style", get_template_directory_uri() . "/style.css" );
        wp_enqueue_script( "AtividadeDaSemana-script", get_template_directory_uri() . "Js/script.js" );
    }

    add_action( "wp_enqueue_scripts", "wordpressAula2_scripts" );

    function console_log($seila){
        echo "<script>console.log(". json_encode($seila) .")</script>";
    }

    add_theme_support( "title-tag" );
    add_theme_support( "menus" );
    add_theme_support( "post-thumbnails" );

    function custom_post_type_noticias() {
        register_post_type("noticia", array(
            "label" => "Notícias",
            "description" => "Descrição Notícias",
            "menu_position" => 2,
            "public" =>true,
            "show_uri" => true,
            "show_in_menu" => true,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "query_var" => true,
            "supports" => array("title", "editor", "thumbnail"),

            "labels" => array (
                "name" => "Notícia",
                "singular_name" => "Notícia",
                "menu_name" => "Notícia",
                "add_new" => "Nova Notícia",
                "add_new_item" => "Adicionar Nova Notícia",
                "edit" => "Editar Notícia",
                "edit_item" => "Editar Notícia",
                "new_item" => "Nova Notícia",
                "view" => "Ver Notícia",
                "view_item" => "Ver Notícia",
                "search_item" => "Procurar Notícia",
                "not_found" => "Nenhuma Notícia Encontrada",
                "not_found_in_trash" => "Nenhuma Notícia Encontrada",

            )
        ));
    }; 
    add_action( "init", "custom_post_type_noticias");
    
?>