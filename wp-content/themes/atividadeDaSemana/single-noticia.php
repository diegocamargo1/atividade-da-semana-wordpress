<?php get_header() ?>

    <main>
        <?php the_post_thumbnail();?>
        <p><?php the_content();?></p>
        <h2>Outras noticias</h2>

    <?php
    $title=the_title("","",false);
        $args = array(
            "post_type"        => "noticia",
            "post_status"      => "publish",
            "suppress_filters" => true,
            "orderby"          => "rand",
            "order"            => "DESC"
        );
 
        $news = new WP_Query( $args );
  
        if( $news->have_posts() ) : 
            $i=0;
            while (($news->have_posts()) && ($i<=3)) : $news->the_post();
                $i=$i+1;
                if($title!=the_title("","",false)):
    ?>
                
                <p><a href="<?php the_permalink();?>"><?php the_title();?></a></p>
    <?php 
                endif;
            endwhile; 
        else:
    ?>
            <p><?php esc_html_e("Não temos custom post");?></p>
    <?php 
        endif;
        echo paginate_links()
    ?>

    </main>

<?php get_footer() ?>