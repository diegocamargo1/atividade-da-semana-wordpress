<?php get_header() ?>

    <main>
        <?php the_post_thumbnail();?>
        <p><?php the_content();?></p>
    </main>

<?php get_footer() ?>