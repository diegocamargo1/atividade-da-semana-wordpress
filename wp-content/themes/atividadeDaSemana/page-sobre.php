<?php get_header() ?>

    <main>
        <p><?php the_content(); ?></p>
        <?php get_template_part( "inc", "section"); ?>
    </main>

<?php get_footer() ?>