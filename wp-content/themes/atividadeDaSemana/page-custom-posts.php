<?php get_header()?>

    <main>
    <form id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <input type="text" class="search-field" name="s" placeholder="Pesquisa" value="<?php echo get_search_query(); ?>">
        <input type="hidden" name="post_type[]" value="noticia">
        <input type="submit" value="Procurar">
    </form>

    <?php
 
        $args = array(
            "post_type"        => "noticia",
            "post_status"      => "publish",
            "suppress_filters" => true,
            "orderby"          => "post_data",
            "order"            => "DESC"
        );
 
        $news = new WP_Query( $args );
  
        if( $news->have_posts() ) : 
            while ( $news->have_posts() ) : $news->the_post();
    ?>
                <h2><?php the_title();?></h2>
                <p><?php the_content();?></p>
                <p><a href="<?php the_permalink();?>">Link</a></p>
    <?php 
            endwhile; 
        else:
    ?>
            <p><?php esc_html_e("Não temos custom post");?></p>
    <?php 
        endif;
        echo paginate_links()
    ?>
    </main>

<?php get_footer()?>