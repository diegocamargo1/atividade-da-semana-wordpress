<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AOx0fT/s0giL4Ri98TNRK+DS7wDSSJ74WCfHfjH5jiJXSrRDd1824hfGFeDz6zxXw0vhSgnilZLdRdB7pBWQcQ==');
define('SECURE_AUTH_KEY',  'hSn4qvc6S9TGkq5HRRfaO5B7xlGRzcb1naTMA7t7UZJZYnMAz4Al4OmhZb2JY11tKTcp8mASa2QJWTags4amLQ==');
define('LOGGED_IN_KEY',    'W+glw6Yqp04gJ5O1DzJNsb8OA13rcbpaRPT3mTIflLuvF1lN83XkPX7II0d0yg1nWsia01dYLwETr7xn98NkZw==');
define('NONCE_KEY',        'vmXBGs/t0WWgEjxxvLuWuyjpvLUWSRLYzJ09pIV8byoGEzp2Alsewl3dO49BNbpHfCgrQZYbKxTEvsq+SrGoSQ==');
define('AUTH_SALT',        'LandynpXMDh+UIUqU/Q8VS0uaVr1+oUMfo+H4eljqc8Nm7OcPL9PrMGfDaonXtTlYGahl8ARZTfgLmG0WiqpAQ==');
define('SECURE_AUTH_SALT', 'oFaP90Ee6K8Wo9m5Z9IvuIwv6+aqFfOuFi8axVkmuwSplG6scVspNJUObADyh+cFbRppKOt4xIEIEd6X4Vb5Aw==');
define('LOGGED_IN_SALT',   'KOBsEweORAv80nEYETZGiZaWCltOhQCbZ8uI655JxQq/Z3twDX3aAb7YKtfAkqRR3JodkdW5M1XpuNTSC/Kgog==');
define('NONCE_SALT',       'GMFVryE735qmUQ6ddkgzGg7wEaKH9iwVvkJdr6Kf7b0xPeqf9pkiWn9tO3TUzb2HfIi6Ge5BvwTAwCCKKO35nQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
